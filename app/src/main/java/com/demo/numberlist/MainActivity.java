package com.demo.numberlist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    private static final int NUMBER_OF_ITEMS = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//      Create recycler view.
        RecyclerView numberRecyclerView = findViewById(R.id.rv_number_list);

        ArrayList<MyNumber> myNumberList = createDummyNumberList(NUMBER_OF_ITEMS);

//      Create an adapter and supply the data to be displayed.
        MyNumberAdapter myNumberAdapter = new MyNumberAdapter(this, myNumberList);

//      Connect the adpater with the recycler view.
        numberRecyclerView.setAdapter(myNumberAdapter);

//      Give the recycler view a default layout manager.
        numberRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    /**
     * Create a dummy list of MyNumber for testing.
     * @param n The number of items.
     * @return An array of MyNumber with n items.
     */

    private ArrayList<MyNumber> createDummyNumberList(int n) {
        ArrayList<MyNumber> dummyNumberList = new ArrayList<MyNumber>();
        
        for (int i = 0; i< n; i++) {
            dummyNumberList.add(new MyNumber(String.valueOf(i)));
        }
        
        return dummyNumberList;
    }
}